const cards = document.querySelectorAll('.card');
var timerShow = document.querySelector('.timer');
var modal = document.querySelector('.popup');
var restartButton = document.querySelector('.restart-button');

var timeSeconds = 60;
var lockBoard = false;
var isDisabled = false;
var isFlippedCard = false;
var firstCard, secondCard;
var timerFlag = false;
var disabledCardsCounter = 0;

cards.forEach(card => card.addEventListener('click', flipCard));
restartButton.addEventListener('click', restart);
shuffle();

function restart() {
	timeSeconds = 60;
	lockBoard = false;
	isDisabled = false;
	isFlippedCard = false;
	timerFlag = false;
	disabledCardsCounter = 0;
	firstCard, secondCard = undefined;
	if (document.querySelector('.label.win').classList.contains('visible'))		
		document.querySelector('.label.win').classList.remove('visible');
	if (document.querySelector('.label.lose').classList.contains('visible'))
		document.querySelector('.label.lose').classList.remove('visible');
	cards.forEach(card => {
	    card.addEventListener('click', flipCard);
	    card.classList.remove('flip');
	    if (card.lastElementChild.classList.contains('red-illumination'))
	    	card.lastElementChild.classList.remove('red-illumination');
	    if (card.lastElementChild.classList.contains('green-illumination'))
	    	card.lastElementChild.classList.remove('green-illumination');
	});

	modal.classList.remove('visible');
	timerShow.innerHTML = '01:00';
	setTimeout(shuffle, 300);
}

function shuffle() {
	cards.forEach(card => {
	    let newPosition = Math.floor(Math.random() * 12);
	    card.style.order = newPosition;
	});
}

function timer() {
	--timeSeconds;
	let timer = setInterval(function () {
	    let seconds = timeSeconds % 60;
	    let minutes = timeSeconds / 60 % 60;
	    if (timeSeconds <= 0) {
	        clearInterval(timer);
	        document.querySelector('.label.lose').classList.add('visible');
	        modal.classList.add('visible');
	    }
	    if (disabledCardsCounter === 12) {
	    	clearInterval(timer);
	        document.querySelector('.label.win').classList.add('visible');  	
			modal.classList.add('visible');
	    }
	    else {
	        let strTimer = formatTime(seconds, Math.trunc(minutes));
	        timerShow.innerHTML = strTimer;
	    }
	    --timeSeconds;
	}, 1000)
}

function formatTime(seconds, minutes) {
	let out = '';
	(minutes < 10) ? out += '0' + minutes + ':' : out += minutes + ':';
	(seconds < 10) ? out += '0' + seconds : out += seconds;
	return out;
}

function flipCard() {
	if (!timerFlag) {
		timer();
	}
	timerFlag = true;
	if (this.classList.contains('flip')) return;

	this.classList.add('flip');

	if (!isFlippedCard) {
		if (firstCard && secondCard && !isDisabled) unflipCards();
		isFlippedCard = true;
		firstCard = this;
		return;
	}

	isFlippedCard = false;
	secondCard = this;

	checkForMatch();
}

function checkForMatch() {
	let isMatch = firstCard.dataset.animal === secondCard.dataset.animal;

	if (isMatch) {
		disableCards();
	}
	else {
		isDisabled = false;
		setTimeout(addIllumination, 100, 'red-illumination');
	}
}


function disableCards() {
	isDisabled = true;
	addIllumination('green-illumination');
	firstCard.removeEventListener('click', flipCard);
	secondCard.removeEventListener('click', flipCard);

	disabledCardsCounter += 2;
}

function unflipCards() {
	firstCard.classList.remove('flip');
	secondCard.classList.remove('flip');
	removeIllumination('red-illumination');
}

function addIllumination(color) {
	firstCard.lastElementChild.classList.add(color);
	secondCard.lastElementChild.classList.add(color);
}

function removeIllumination(color) {
	firstCard.lastElementChild.classList.remove(color);
	secondCard.lastElementChild.classList.remove(color);
}